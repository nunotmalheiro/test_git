/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.teste01;

/**
 *
 * @author user
 */
public class Invoice {
    private String id;
    public Invoice(String id) {
        this.id = id;
    }
    
    public int calc(Integer i) {
        return id.hashCode()+i;
    }
    
    public int newCalc(Integer i) {
        return id.hashCode()*i;
    }    
}
